package App::Exceptions::DB::RetrieveAll;

use Moose;
extends 'App::Exceptions';
with 'Throwable';
has '+message' => (
    is => 'rw',
    isa => 'Str',
    lazy => 1,
    default => sub { 'retrieve all error (DB)' }
);

__PACKAGE__->meta->make_immutable();