package App::Exceptions::DB::Duplicate;

use Moose;
extends 'App::Exceptions';
with 'Throwable';
has '+message' => (
    is => 'rw',
    isa => 'Str',
    lazy => 1,
    default => sub { 'duplicate register (DB)' }
);

__PACKAGE__->meta->make_immutable();