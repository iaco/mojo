package App::Exceptions::DB::Update;

use Moose;
extends 'App::Exceptions';
with 'Throwable';
has '+message' => (
    is => 'rw',
    isa => 'Str',
    lazy => 1,
    default => sub { 'update command error (DB)' }
);

__PACKAGE__->meta->make_immutable();