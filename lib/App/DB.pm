package App::DB;

use Moose;
use MooseX::ClassAttribute;
use DBI;
use SQL::Abstract;

# Exceptions
use App::Exceptions::DB::Create;
use App::Exceptions::DB::RetrieveAll;
use App::Exceptions::DB::Duplicate;
use App::Exceptions::DB::Update;

use Data::Dumper;

extends 'App';

use constant CONFIG_FILE => 'config/app.json';

class_has 'dbh' => (
    is => 'ro',
    isa => 'DBI',
    default => sub {
        my $config = App->parse_json_file( CONFIG_FILE );
        DBI->connect("dbi:mysql" .
            ":" . $config->{database}->{database} .
            ":" . $config->{database}->{hostname} . ":3306", "","") or die "Could not connect " . @!;
    }
);

class_has 'sqlAbs' => (
    is => 'ro',
    isa => 'DBI',
    default => sub {
        SQL::Abstract->new();
    }
);

sub create {
    my ( $self, $obj ) = @_;

    my $class = ref $self;
    my $store = $self->store;
    my $meta = $class->meta;

    my %data;
    for my $attr ( $meta->get_all_attributes ) {
        next if $attr->name =~ /^_/;
        my $value = $attr->name;
        $data{$value} = $self->$value;
    }

    my ( $stmt, @bind ) = $self->sqlAbs->insert( $store, \%data );
    my $sth = $self->dbh->prepare( $stmt );
    $sth->execute( @bind ) ? return $sth->{mysql_insertid} : App::Exceptions::DB::Create->throw();
}

sub retrieve {
    my ( $self, $data ) = @_;

    my ( $stmt, @bind ) = $self->sqlAbs->select( $self->store, '*', $data );
    my $sth = $self->dbh->prepare( $stmt );
    $sth->execute( @bind );
    my $result = $sth->fetchrow_hashref;

    if ( $result ) {
        my $object = $self->new( $result );
        return $object;
    } else {
        return undef;
    }
}

sub update {
    my ( $self, $data, $where ) = @_;

    my ( $stmt, @bind ) = $self->sqlAbs->update( $self->store, $data, $where );
    my $sth = $self->dbh->prepare( $stmt );
    $sth->execute( @bind ) ? return $sth->{mysql_insertid} : App::Exceptions::DB::Update->throw();
}

sub delete {
    my ( $self, $where ) = @_;

    my ( $stmt, @bind ) = $self->sqlAbs->delete( $self->store, $where );
    my $sth = $self->dbh->prepare( $stmt );
    $sth->execute( @bind ) ? return $sth->{mysql_insertid} : App::Exceptions::DB::Delete->throw();
}

sub check_dups {
    my ( $self, $data ) = @_;

    $self->retrieve( $data ) ? App::Exceptions::DB::Duplicate->throw() : return 0;
}

sub retrieve_all {
    my ( $self, $data ) = @_;

    my ( $stmt, @bind ) = $self->sqlAbs->select( $self->store, '*', $data );
    my $sth = $self->dbh->prepare( $stmt );
    $sth->execute( @bind );
    my $result = $sth->fetchall_arrayref({});

    if ( $result ) {
        my @collection;
        foreach my $obj ( @$result ){
            push @collection, $self->new( $obj )->to_hash;
        }
        return \@collection;
    } else {
        App::Exceptions::DB::RetrieveAll->throw();
    }
}

sub to_hash {
    my $self = shift;

    my $class = ref $self;
    my $meta = $class->meta;

    my $data = {};
    for my $attr ( $meta->get_all_attributes ) {
        next if $attr->name =~ /^_/;
        my $value = $attr->name;
        if ($attr->type_constraint eq 'Bool') {
            $data->{$value} = $self->$value ? \1 : \0;
        } elsif ($attr->type_constraint eq 'Num' && $self->$value ) {
            $data->{$value} = $self->$value+0;
        } else {
            $data->{$value} = $self->$value;
        }
    }

    return $data;
}

__PACKAGE__->meta->make_immutable();