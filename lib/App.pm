package App;

use Moose;
use MooseX::ClassAttribute;
use JSON::XS;

class_has 'logger' => (
    is => 'rw'
);

class_has 'logged_user' => (
    is => 'rw',
    # isa => 'Maybe[Gympro::Model::User]'
);

class_has 'config' => (
    is => 'rw',
    isa => 'HashRef'
);

sub parse_json_file {
    my $self = shift;
    my $file = shift;

    if (-e $file) {
        my $string = do {
           open my $json_fh, "<:encoding(UTF-8)", $file or die $!;
           local $/;
           <$json_fh>
        };

        my $json = JSON::XS->new;
        $json->relaxed(1);
        $json->space_after(1);

        return $json->decode( $string );
    } else {
        return {}
    }

}

sub database_config {
    my $self = shift;
    my $value = shift;
    return $self->_from_config('database',$value);
}

sub secret {
    my $self = shift;
    return $self->_from_config('app','secret');
}

sub expiration {
    my $self = shift;
    return $self->_from_config('app','expiration_time');
}

sub _from_config {
    my $self = shift;
    my $node = shift || die;
    my $key = shift || die;

    return $self->config->{$node}->{$key}
}

__PACKAGE__->meta->make_immutable();